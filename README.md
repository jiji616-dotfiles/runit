# runit

Repository for user services for runit.

Available services:
* bar
* compton
* dbus
* dunst
* dwm
* mopidy
* mpdscribble
* redshift
* ssh-agent
* taskd
* transmission
* weechat
* xbanish
* xcape

## How to use

These are services for an X session so it's best to start them in xinit.
At the end of your `.xinitrc` file instead of execing into your window manager launch runsvdir:
```
exec runsvdir "$HOME"/.local/run &> .local/log/userlog
```
I keep my services in $HOME/.local/sv so if I want to add and launch a new one I just symlink it to $HOME/.local/run and runsvdir picks it up. For managing user services I use a simple alias:
```
alias lsv="SVDIR=$HOME/.local/run sv"
```
